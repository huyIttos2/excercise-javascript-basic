const {
  addStudentService,
  editStudentService,
  deleteStudentService,
  getStudentsService,
  getStudentService,
} = require('./src/student/student_service');

const classServices = require('./src/class/class_service');

const main = async () => {
  try {
    // const result = classServices.addClassService(
    //   'NodeJS',
    //   '2021-05-05',
    //   '2021-05-06'
    // );

    // const result = await classServices.editClassService('5680314351623405055204', 'JavaScript')
    // const result = await classServices.deleteClassService('1390172191623406870137')
    // const result = await classServices.getClassService('7887349681623406866027');
    const result = await classServices.getClassesService('NodeJS', '2021-05-05');

return result;
  } catch (err) {
    console.log(err.message);
  }
};

main().then(r => console.log(r)).catch(e => console.log(e));