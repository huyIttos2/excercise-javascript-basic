const studentDAL = require('./student_dal');
const helpers = require('../../helper');

const addStudentService = (firstName, lastName, classId, dob, address, conduct) => {
  if (!firstName) {
    throw Error('firstName is required');
  }

  if (!lastName) {
    throw Error('lastName is required');
  }

  if (!classId) {
    throw Error('classId is required');
  }

  if (!dob) {
    throw Error('dob is required');
  }

  if (!address) {
    throw Error('address is required');
  }

  if (conduct && !['bad', 'normal', 'good'].includes(conduct)) {
    throw Error('conduct must be bad, normal or good');
  }

  const id = helpers.generateId();
  const classObject = {
    id,
  };

  return classObject;
};

const editStudentService = (id) => {};

const deleteStudentService = (id) => {};

const getStudentService = (id) => {};

const getStudentsService = (textSearch) => {};

module.exports = {
  addStudentService,
  editStudentService,
  deleteStudentService,
  getStudentService,
  getStudentsService,
};