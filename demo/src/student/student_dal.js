const fs = require('fs-extra');

const path = './student_dummy.json';

const saveStudent = (payload) => {
  return fs.writeFile(path, payload);
};

const getStudents = () => {
  return fs.readFile(path);
}

const getOneStudent = () => {
}

module.exports = {
  saveStudent,
  getStudents,
  getOneStudent,
}