/**
 * This function is a dummy database
 */
const fs = require('fs-extra');
const path = require('path');
const pathToFile = path.resolve(__dirname, './class_dummy.json');

const create = async (payload) => {
  let allClasses = await getMany();
  allClasses = allClasses;
  allClasses.push(payload);

return fs.writeFile(pathToFile, JSON.stringify(allClasses));
};

const update = async (id, payload) => {
  let allClasses = await getMany();
  allClasses = allClasses;
  const classIndex = allClasses.findIndex((_class) => _class.id === id);
  if (!allClasses[classIndex]) {
    throw Error('Class not found');
  }
  allClasses[classIndex] = payload;

return fs.writeFile(pathToFile, JSON.stringify(allClasses));
};

const remove = async (id) => {
  let allClasses = await getMany();
  allClasses = allClasses;
  const classIndex = allClasses.findIndex((_class) => _class.id === id);
  if (!allClasses[classIndex]) {
    throw Error('Class not found');
  }
  allClasses.splice(classIndex, 1); // Remove 1 item in array

return fs.writeFile(pathToFile, JSON.stringify(allClasses));
};

const getMany = async (filterObject) => {
  const lsAll = await fs.readFile(pathToFile);
  let allItems = JSON.parse(lsAll);

  if (filterObject) {
    const allKeysFilter = Object.keys(filterObject);

    allItems = allItems.filter((item) => {
      let match = true;
      allKeysFilter.forEach((key) => {
        if (item[key] !== filterObject[key]) {
          match = false;
        }
      });

      return match;
    });
  }

  return allItems;
};

const getOne = async (id) => {
  const allClasses = await getMany();
  const classObject = JSON.parse(allClasses).find((_class) => _class.id === id);

return classObject;
};

module.exports = {
  create,
  update,
  remove,
  getMany,
  getOne,
};