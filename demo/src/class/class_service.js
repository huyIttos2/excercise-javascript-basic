const Class = require('./class_dal');
const helpers = require('../../helper');

/**
 * Create new class
 * @param {String} name class's name
 * @param {Date} startDate datetime kick off the class
 * @param {Date} endDate datetime close the class
 * @returns {*} Any
 */
const addClassService = (name, startDate, endDate) => {
  if (!name) {
    throw Error('name is required');
  }

  if (!startDate) {
    throw Error('startDate is required');
  }

  if (!helpers.isDate(startDate)) {
    throw Error(`startDate must be ${helpers.datetimeFormat} format`);
  }

  if (endDate && !helpers.isDate(endDate)) {
    throw Error(`endDate must be ${helpers.datetimeFormat} format`);
  }

  if (endDate && new Date(endDate) < new Date(startDate)) {
    throw Error('endDate must be later than startDate');
  }

  const id = helpers.generateId();
  const classObject = {
    id,
    name,
    startDate,
    endDate
  };

  return Class.create(classObject);
};

const editClassService = async (id, name, startDate, endDate) => {
  if (!id) {
    throw Error('id is required');
  }

  const classObject = await Class.getOne(id);
  if (!classObject) {
    throw Error('Class not found');
  }

  if (name) {
    classObject.name = name;
  }

  if (startDate) {
    if (!helpers.isDate(startDate)) {
      throw Error(`startDate must be ${helpers.datetimeFormat} format`);
    }

    classObject.startDate = startDate;
  }

  if (endDate) {
    if (!helpers.isDate(endDate)) {
      throw Error(`endDate must be ${helpers.datetimeFormat} format`);
    }

    classObject.endDate = endDate;
  }

  if (classObject.endDate && new Date(classObject.endDate) < new Date(classObject.startDate)) {
    throw Error('endDate must be later than startDate');
  }
  console.log(classObject);
  return Class.update(id, classObject);
};

const deleteClassService = (id) => {
  if (!id) {
    throw Error('id is required');
  }

  return Class.remove(id);
};

const getClassService = async (id) => {
  if (!id) {
    throw Error('id is required');
  }

  const classObject = await Class.getOne(id);

  if (!classObject) {
    throw Error('Class not found')
  }

  return classObject;
};

const getClassesService = async (textSearch, startDate) => {
  const filter = {};
  if (textSearch) {
    filter.name = textSearch;
  }

  if (startDate) {
    filter.startDate = startDate;
  }

  const lsClasses = await Class.getMany(filter);

  return lsClasses;
};

module.exports = {
  addClassService,
  editClassService,
  deleteClassService,
  getClassService,
  getClassesService,
}