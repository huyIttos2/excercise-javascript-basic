
SETUP

1, Install node_modules
  > npm i

2, Install Eslint extension
  > Shortcut: Shift + Cmd + X
  > Search: Eslint
  > Install and reload VSCode

3, Run
  > node index.js
