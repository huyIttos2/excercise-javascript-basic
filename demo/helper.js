const moment = require('moment');

// Configuration variable
const randomNumberLength = 9;

/**
 * This is simple function to gnerate a random id
 * @returns {String} id Id is random string combine by a random number and current timestamp
 */
exports.generateId = () => {
  const randomNumber = Math.floor(Math.random() * Math.pow(10, randomNumberLength));
  const timestamp = Date.now();

  return `${randomNumber}${timestamp}`;
};

const datetimeFormat = 'YYYY-MM-DD';

/**
 * Check datetime format based on datetimeFormat configuration
 * @param {*} date
 * @returns {Boolean} true if date is correct format, false if not
 */
exports.isDate = (date) => {
  return moment(date, datetimeFormat, true).isValid();
};

exports.datetimeFormat = datetimeFormat;
