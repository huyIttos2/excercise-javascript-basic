import {
  getDataFromFile,
  makeid,
  formatDate,
  isDate,
  getParams,
  writeData,
} from '../helper.js';

const pathClass = new URL('./data/classes.json', import.meta.url);
const pathStudent = new URL('./data/student.json', import.meta.url);

/*
 * @description Add a class
 */
const addClass = async (id, name, startDate, endDate) => {
  const classData = await getDataFromFile(pathClass);
  const classFindById = classData.find((item) => item.id === id);

  if (classFindById) {
    return;
  }
  let data = {};
  data = {
    id: makeid(5),
    name,
    startDate: formatDate(startDate),
    endDate: formatDate(endDate),
  };
  classData.push(data);
  await writeData(pathClass, classData);
};

/*
 * @description Update class by id
 */
const updateClassById = async (id, data) => {
  Object.keys(data).length === 0 && data.constructor === Object;
  const params = {};
  params.name = getParams(data, 'name', '');
  params.startDate = getParams(data, 'startDate', '');
  params.endDate = getParams(data, 'endDate', '');
  const classData = await getDataFromFile(pathClass);
  const { name, startDate, endDate } = params;
  const classFindById = classData.find((item) => item.id === id);
  const classIndex = classData.findIndex((item) => item.id === id);

  if (!classFindById) {
    return;
  }

  const endDateFormated = formatDate(endDate);
  const startDateExists = classFindById.startDate;

  if (endDate && Date.parse(endDateFormated) < Date.parse(startDateExists)) {
    return;
  }

  const nameUpdated = name !== '' ? name : classFindById.name;
  const startDateUpdated = isDate(startDate)
    ? formatDate(startDate)
    : classFindById.startDate;
  const endDateUpdated = isDate(endDate)
    ? formatDate(endDate)
    : classFindById.endDate;
  const classUpdated = {
    ...classFindById,
    name: nameUpdated,
    startDate: startDateUpdated,
    endDate: endDateUpdated,
  };
  classData[classIndex] = classUpdated;
  await writeData(pathClass, classData);
};

/*
 * @description Delete class by id
 */
const deleteClassById = async (id) => {
  const classData = await getDataFromFile(pathClass);
  const classFind = classData.find((item) => item.id === id);
  const classFindIndex = classData.find((item) => item.id === id);
  if (classFind) {
    // const classUpdate = classData.filter((item) => item.id !== id);
    classData.splice(classFindIndex, 1);
    writeData(pathClass, classUpdate);
    return classFind;
  }
};

/*
 * @description Get list classes
 */
const getListClasses = async (condition) => {
  let filterClasses = [];
  const classData = await getDataFromFile(pathClass);
  const students = await getDataFromFile(pathStudent);

  if (Object.keys(condition).length === 0 && condition.constructor === Object) {
    filterClasses = classData.slice();
  }

  const params = {};
  params.name = getParams(condition, 'name', '');
  const { name } = params;

  if (name) {
    // filterClasses = classData.filter((item) => item.name.includes(name));
    filterClasses = classData.reduce((acc, cur, index) => {
      if (cur.name.includes(params.name)) {
        const classId = cur.id;
        const studentInClass = students.filter(
          (item) => item.classId === classId
        );
        const toltalStudent = studentInClass.length;
        const curUpdate = {
          ...cur,
          toltalStudent,
        };
        classData[index] = curUpdate;
        return [...acc, curUpdate];
      }

      return acc;
    }, []);
  }
  console.log(filterClasses);
};

const getClassDetails = async () => {};

export {
  addClass,
  updateClassById,
  deleteClassById,
  getListClasses,
  getClassDetails,
};
