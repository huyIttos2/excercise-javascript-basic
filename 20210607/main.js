import { addStudent, updateStudentById, deleteStudentById, getListStudent, getStudentDetails } from './students-service.js';

import { addClass, updateClassById, deleteClassById, getListClasses, getClassDetails } from './class-services.js';

// addClass('', 'Node-JS', new Date(2021, 1, 19), new Date(2021, 3, 19));
// addClass('', 'React-JS', new Date(2021, 2, 1), new Date(2021, 5, 1));
// addClass('', 'Vue-JS', new Date(2021, 4, 1), new Date(2021, 6, 1));
// addClass('', 'PHP', new Date(2021, 4, 1), new Date(2021, 6, 1));
// addClass('', 'JAVA', new Date(2021, 4, 1), new Date(2021, 6, 1));

// addStudent({ id: '', firstName: 'huy', lastName: 'nguyen', classId: 'XofTP', dob: new Date(1995, 11, 7), address: 'Ha Noi', conduct: 'Normal' });
// addStudent({ id: '', firstName: 'hung', lastName: 'Tran', classId: 'XofTP', dob: new Date(1995, 12, 7), address: 'Vinh Phuc', conduct: 'Normal' });
// addStudent({ id: '', firstName: 'Cuong', lastName: 'Phan', classId: 'XofTP', dob: new Date(11997, 12, 7), address: 'Ha Nam' });

// deleteStudentById('RZiBY');

// getListStudent({ lastName: 'an' });
// getListStudent({});
// updateStudentById('LjMvR', {});
// updateStudentById('e0tyh', { lastName: 'nguyen', classId: 'bU1EJ' });

// updateClassById('bU1EJ', { name: 'Java-update', startDate: new Date(2021, 1, 1), endDate: new Date(2021, 1, 2) });

// deleteClassById('bU1EJ');
// getListClasses({ name: 'Node' });
