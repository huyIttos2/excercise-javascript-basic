import fs from 'fs';

/**
 * This is function read data from json file
 * @returns {Array} list all data here is all classes existed in json files
 */
const getDataFromFile = async (p) => {
  const fsPromises = fs.promises;

  const data = await fsPromises
    .readFile(p)
    .catch((err) => console.error('Failed to read file', err));

  return JSON.parse(data.toString());
};

/*
 * @description Save data to file json
 */
const writeData = async (p, data) => {
  const fsPromises = fs.promises;
  await fsPromises
    .writeFile(p, JSON.stringify(data))
    .catch((err) => console.log('Failed to write file', err));
};

/**
 * This is function read data from json file
 * @returns {string} this is simple function generate a strings random with fixed length
 */
const makeid = (length) => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

/**
 * This is function read data from json file
 * @returns {string} this is simple function generate a strings random with fixed length
 */
const formatDate = (d) => {
  const dateString = d.toISOString().split('T')[0];
  return dateString;
};
const getParams = (params, property, defaultValue) => {
  if (params.hasOwnProperty(property) && params[property] !== undefined) {
    return params[property];
  }
  return defaultValue;
};

/*
 * @description Check inputDate is Date object
 */
const isDate = (input) => {
  if (Object.prototype.toString.call(input) === '[object Date]') {
    return true;
  }
  return false;
};

export { getDataFromFile, makeid, formatDate, getParams, writeData, isDate };
