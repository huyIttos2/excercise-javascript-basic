import {
	getDataFromFile,
	makeid,
	getParams,
	formatDate,
	writeData,
} from '../helper.js';

const pathClass = new URL('./data/classes.json', import.meta.url);
const pathStudent = new URL('./data/student.json', import.meta.url);

 /* 
 * @description Add a student
 */
const addStudent = async (data) => {
	const classExist = await getDataFromFile(pathClass);
	const classIdExist = classExist.map((item) => item.id);
	const {
		id,
		firstName,
		lastName,
		classId,
		dob,
		address,
		conduct,
	} = data;
	const students = await getDataFromFile(pathStudent);
	const studentFindById = students.find((item) => item.id === id);

	if (studentFindById) {
		return;
	}

	if ( typeof firstName === 'undefined' || typeof lastName === 'undefined' || typeof classId === 'undefined' ||
	 typeof dob === 'undefined'|| typeof address === 'undefined'
	) {
		return;
	}

	if (classIdExist.indexOf(classId) < 0) {
		return;
	}

	const student = {
		id: makeid(5),
		firstName,
		lastName,
		classId,
		dob: formatDate(dob),
		address,
		conduct,
	};
	students.push(student);
	await writeData(pathStudent, students);
};


/* 
* @description Update student by id
*/
const updateStudentById = async (id, data) => {
	const students = await getDataFromFile(pathStudent);

	if (Object.keys(data).length === 0 && data.constructor === Object) {
		return;
	}

	const student = students.find((student) => student.id === id);
	const studentIndex = students.findIndex((student) => student.id === id);
	const classExist = await getDataFromFile(pathClass);
	const classIdExist = classExist.map((item) => item.id);

	if (!student) {
		return;
	}

	const classId = data.hasOwnProperty('classId') ? data.classId : null;

	if (classId !== null && classIdExist.indexOf(classId) < 0) {
		return;
	}

	const studentUpdate = {
		...student,
		firstName: data.firstName || student.firstName,
		lastName: data.lastName || student.lastName,
		classId: data.classId || student.classId,
	};
	students[studentIndex] = studentUpdate;
	await writeData(pathStudent, students);
	console.log(students);
};

/* 
* @description Delete student by id
*/
const deleteStudentById = async (id) => {
	const students = await getDataFromFile(pathStudent);
	const studentIndex = students.findIndex((item) => item.id === id);
	// const studentUpdated = students.filter((student) => student.id !== id);
	students.splice(studentIndex, 1);
	await writeData(pathStudent, students);
};

/* 
* @description Get list students
*/
const getListStudent = async (condition) => {
	let studentFiltered = [];
	const students = await getDataFromFile(pathStudent);

	if (Object.keys(condition).length === 0 && condition.constructor === Object) {
		studentFiltered = students.slice();
	}

	const params = {};
	params.lastName = getParams(condition, 'lastName', '');
	params.classId = getParams(condition, 'classId', '');
	const { lastName, classId } = params;

	if (lastName && classId) {
		studentFiltered = students.reduce((acc, cur, index) => {
			if (cur.lastName.includes(lastName) && cur.classId.includes(classId)) {
				const fullName = `${cur.firstName} ${cur.lastName}`;
				const curUpdate = {
					...cur,
					fullName,
				};
				students[index] = curUpdate;
				return [...acc, curUpdate];
			}

			return acc;
		}, []);
	} else if (lastName) {
		studentFiltered = students.filter((student) => student.lastName.includes(lastName));
	} else if (classId) {
		studentFiltered = students.filter((student) => student.classId.includes(classId));
	}
	console.log(studentFiltered);
};

const getStudentDetails = async () => {};

export {
	addStudent,
	updateStudentById,
	deleteStudentById,
	getListStudent,
	getStudentDetails,
};

