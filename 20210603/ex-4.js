const numbers = [4, 2, 5, 1, 2, 3, 5, 9, 8, 1];

const reducerDuplicateElement = (acc, cur) => {
  const indexOfCur = acc.findIndex((e) => e === cur);
  if (indexOfCur < 0) {
    return [...acc, cur];
  }
  return acc;
};

const numbersUpdated = numbers.reduce(reducerDuplicateElement, []);

console.log(numbersUpdated);
