const numbers = [4, 2, 5, 1, 2, 3, 5, 9, 8, 1];

// use sort function built in javascript
const numberSorted = numbers.sort((a, b) => a - b);
console.log(numberSorted);

// use for to sort array ascending
const sortAsc = (arr) => {
  const arrLength = arr.length;
  for (let i = 0; i < arrLength; i += 1) {
    for (let j = arrLength - 1; j >= i; j += -1) {
      if (arr[j] <= arr[j - 1]) {
        const tmp = arr[j];
        arr[j] = arr[j - 1];
        arr[j - 1] = tmp;
      }
    }
  }
  return arr;
};

console.log(sortAsc(numbers));
