const getTotalObjectValue = (obj) => {
  let valuesArrays = [];
  if (Object.keys(obj).length === 0 && obj.constructor === Object) {
    return;
  }
  valuesArrays = Object.values(obj);

  return valuesArrays;
};

const cb = (arr) => {
  let sum = 0;
  for (const value of arr) {
    sum += value;
  }
  const promise = new Promise((resolve, reject) => {
    resolve(sum);
  });
  return promise;
};
// @desc with callback function
// const getTotalArrayValue = (arr, cb) => cb(arr);

// with Promise constructor
const getTotalArrayValue = (arr) => {
  cb(arr).then((sum) => {
    console.log(`Sum of test: ${sum}`);
  }).catch((err) => {
    console.log(err);
  });
};

const objTest = {
  a: 1, b: 2, c: 9, d: 6, e: 8,
};

const valueArraysTest = getTotalObjectValue(objTest);

const sumOfTest = getTotalArrayValue(valueArraysTest);
