const numbers = [4, 2, 5, 1, 2, 3, 5, 9, 8, 1];

let sumOfNumbers = 0;

const reducer = (acc, cur) => acc + cur;

sumOfNumbers = numbers.reduce(reducer);

console.log(sumOfNumbers);
