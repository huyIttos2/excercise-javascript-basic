const numbers = [1, 23, 5, 8, 9, 44];

const numbersIncrementByOne = numbers.map((number) => number + 1);

console.log(numbersIncrementByOne);

const person = {
  name: 'huy',
  age: 18,
  email: 'huy@example.com',
  description: 'live in Ha noi',
};
