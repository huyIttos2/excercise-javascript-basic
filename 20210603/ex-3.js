const numbers = [4, 2, 5, 1, 2, 3, 5, 9, 8, 1];

const numbersLength = numbers.length;
const oddNumbers = [];
const evenNumbers = [];

// numbers.forEach((e) => {
//   if (e % 2 === 0) {
//     evenNumbers.push(e);
//   } else {
//     oddNumbers.push(e);
//   }
// });
let i = 0;

while (i < numbersLength) {
  if (numbers[i] % 2 === 0) {
    evenNumbers.push(numbers[i]);
  } else {
    oddNumbers.push(numbers[i]);
  }
  i += 1;
}

console.log(oddNumbers, evenNumbers);
